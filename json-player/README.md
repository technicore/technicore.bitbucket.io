# JSON music format #

Started 2020-10-09.

### What is this repository for? ###

* This repo brings a new JSON-based music format with a strong formal basis, allowing for modularity, readability (compared to MIDI) and... just intonation.
* There's an HTML5 player for the format, using Web Audio API.
* There are also generators for this format.

### Who do I talk to? ###

* Contact me at hecho@live.co.uk
